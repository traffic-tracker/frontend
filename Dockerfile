FROM node:alpine


# Set Environment variables
ENV appDir /var/www/app

RUN apk add --no-cache make gcc g++ python bash bzr git subversion openssh-client ca-certificates

# Set the work directory
RUN mkdir -p ${appDir}
WORKDIR ${appDir}

COPY . ${appDir}
COPY package.json ${appDir}/package.json

# Install npm dependencies and install ava globally
RUN npm install
RUN npm install -g ava
RUN npm rebuild node-sass

ENV TRAFFIC_URL=https://api.traffic.nxtgen.app
ENV TRAFFIC_INTERVAL=5
CMD [ "npm", "run", "dev" ]
